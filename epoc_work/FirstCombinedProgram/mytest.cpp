
#include <iostream>
#include <vector>

#include "eegheadset.h"

using namespace std;


void printVector(const vector<double> &vec)
{
	cout << "[";
	for (int i = 0; i < vec.size(); i++)
		cout << vec[i] << ",";
	cout << "]" << endl;
	return;
}

int main()
{
	EEGHeadset hs;
	
	cout << "ok, trying to connect now." << endl;

	while (!hs.connect())
		cout << "still trying to connect haha." << endl;
	cout << "ok connected, now trying to read data." << endl;

	while (hs.checkConnected())
	{
		vector<vector<double>> *dat = hs.getData();
		if (dat->at(0).size() > 0)
		{
			cout << "processed " << dat->at(0).size() << " lol" << endl;
			printVector(dat->at(3));
		}
	}

	if (hs.checkConnected())
		cout << "The headset has been disconnected." << endl;

	return 0;
}



