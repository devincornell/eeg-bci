#ifndef EEGHEADSET_H
#define EEGHEADSET_H

#include <vector>

// eeg headset includes
#include "../include/EmoStateDLL.h"
#include "../include/edk.h"
#include "../include/edkErrorCode.h"

// these are all the channels that are being recorded
EE_DataChannel_t targetChannelList[] = {
	ED_COUNTER,
	ED_AF3, ED_F7, ED_F3, ED_FC5, ED_T7,
	ED_P7, ED_O1, ED_O2, ED_P8, ED_T8,
	ED_FC6, ED_F4, ED_F8, ED_AF4, ED_GYROX, ED_GYROY, ED_TIMESTAMP,
	ED_FUNC_ID, ED_FUNC_VALUE, ED_MARKER, ED_SYNC_SIGNAL
};


class EEGHeadset
{
private:
	// EmoEngine variables
	EmoEngineEventHandle eEvent;
	EmoStateHandle eState;
	DataHandle hData;
	
	// headset information
	unsigned int userID;
	bool headset_connected;
	unsigned int num_chan;

public:
	// can be accessed by the user
	std::vector<std::vector<double>> data_buffer;
	unsigned int num_samples;

public:
	/*** big 3 ***/
	EEGHeadset();

	~EEGHeadset();

	//EEGHeadset& operator=(const EEGHeadset& anotherHeadset);

	/*** ---- Accessor Operations ---- */

	bool connect(const int timeout);

	bool checkConnected();

	std::vector<std::vector<double>> *getData();

private:

	void processEvents();

};


#include "eegheadset.hpp"

#endif
