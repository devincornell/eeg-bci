
#ifndef EEGHEADSET_HPP
#define EEGHEADSET_HPP

// system includes
#include <exception>
#include <ctime>
#include <conio.h>


// eeg headset includes
#include "../include/EmoStateDLL.h"
#include "../include/edk.h"
#include "../include/edkErrorCode.h"

#pragma comment(lib, "../lib/edk.lib")

EEGHeadset::EEGHeadset()
{
	if (EE_EngineConnect() != EDK_OK) {
		throw std::exception("Emotiv Engine start up failed.");
	}

	// init session state variables
	eEvent = EE_EmoEngineEventCreate();
	eState = EE_EmoStateCreate();
	hData = EE_DataCreate();

	// set buffer size in seconds
	float secs = 20;
	EE_DataSetBufferSizeInSec(secs);

	// init variables
	headset_connected = false;
	num_chan = sizeof(targetChannelList) / sizeof(ED_COUNTER);
	userID = 0;

	// pre-allocate enough columns for all the channels
	std::vector<double> tmp(50);
	for (int i = 0; i < num_chan; i++)
		data_buffer.push_back(tmp);

}


EEGHeadset::~EEGHeadset()
{
	EE_EngineDisconnect();
	EE_EmoStateFree(eState);
	EE_EmoEngineEventFree(eEvent);
}

bool EEGHeadset::connect(const int timeout = 5)
{
	
	// initialize time for timeout
	time_t start_time = time(nullptr);
	time_t curr_time = start_time;
	
	// loop until time runs out, or the headset connected
	while (!_kbhit() && (int)(curr_time - start_time) < timeout && !headset_connected) {

		// check if any headsets have connected, and set them up if they did
		processEvents();
		
		// update current time to check for timeout
		curr_time = time(nullptr);
	}

	return headset_connected;
}

bool EEGHeadset::checkConnected()
{
	return headset_connected;
}

void EEGHeadset::processEvents()
{
	int state = EE_EngineGetNextEvent(eEvent);
	
	// keep going as long as there are events to process
	while (state == EDK_OK)
	{
		EE_Event_t eventType = EE_EmoEngineEventGetType(eEvent);
		switch (eventType) {
		case EE_UserAdded:
			EE_EmoEngineEventGetUserId(eEvent, &userID);
			EE_DataAcquisitionEnable(userID, true);
			headset_connected = true;
			break;
		case EE_UserRemoved:
			headset_connected = false;
			break;
		default:
			break;
		}
		
		// see if there is another event
		state = EE_EngineGetNextEvent(eEvent);
	}
	return;
}


std::vector<std::vector<double>> *EEGHeadset::getData()
{
	// make sure headset is still connected
	processEvents();
	if (!headset_connected)
	{
		throw std::exception("Can't access headset because it is not connected.");
	}
	else
	{
		// get number of samples that were collected
		EE_DataUpdateHandle(0, hData);
		num_samples = 0;
		EE_DataGetNumberOfSample(hData, &num_samples);

		// clear data in each channel
		for (int i = 0; i < data_buffer.size(); i++)
			data_buffer[i].clear();

		if (num_samples > 0) {

			// copy data into data buffer and then into data vector
			double* data = new double[num_samples];
			for (int i = 0; i < num_chan; i++) {

				// get data from EmoEngine
				EE_DataGet(hData, targetChannelList[i], data, num_samples);

				// put data in the data buffer
				data_buffer[i].insert(data_buffer[i].end(), &data[0], &data[num_samples]);

			}
			delete[] data;
		}
	}

	return &data_buffer;
}




#endif


