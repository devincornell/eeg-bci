function [] = activateTools()
%ACTIVATETOOLS Summary of this function goes here
%   Detailed explanation goes here

% install biosig toolbox
run('tools/biosig4octmat-2.92/biosig_installer.m');

% install custom neural network libraries
addpath(genpath('tools/ConvNet/'));
addpath(genpath('tools/mlp/'));

end

