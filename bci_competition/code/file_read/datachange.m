bci_iv_2a = struct();

nos = 1:9;

for i = nos
    % build the 
    session = struct();
	[session.s, session.info] = sload(['BCICIV_2a_gdf/A0' num2str(i) 'T.gdf']);
    
    save(['A0' num2str(i) 'T.mat'], 'series');
    
    %bci_iv_2a = setfield(bci_iv_2a, ['A0' num2str(i) 'T'], series);
    
    session = struct();
	[session.s, session.info] = sload(['BCICIV_2a_gdf/A0' num2str(i) 'E.gdf']);
    
    save(['A0' num2str(i) 'E.mat'], 'series');
    
    %bci_iv_2a = setfield(bci_iv_2a, ['A0' num2str(i) 'E'], series);
    
end
