function [info, signal] = gdfFileRead(filename)
%GDFFILEREAD Summary of this function goes here
%   Detailed explanation goes here

[signal,info] = sload(filename);

if isempty(signal)
    disp(['Failed to load ' filename]);
    error('The file could not be found. Please give a correct absolute path.');
end

signal(isnan(signal)) = 0;

%sload('G:\projects\eeg-bci\data\BCICIV_2a_gdf\train\A01T.gdf');

end

