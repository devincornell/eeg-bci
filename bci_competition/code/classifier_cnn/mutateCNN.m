function [cnnOut] = mutateCNN(cnnA,cnnB,nP)
%MUTATECNN Summary of this function goes here
%   Detailed explanation goes here

cnnOut = cnnA;

% generate random noise
epsilon = randn(size(cnnA.weights))*nP;

% update weights by taking the mean of the two nn and adding noise
cnnOut.weights = (cnnA.weights + cnnB.weights)/2 + epsilon;

end

