function [cnn,trainerr,iterCompleted] = trainCNNWrap(classifierParams,X,y)
%TRAINCNNWRAP Summary of this function goes here
%   Detailed explanation goes here

genSeed = clock();
genSeed = floor(genSeed(end));

cnn = newCNNWrap(size(X(:,:,1)),size(y,2),genSeed);



for i = 1 : classifierParams.trainIterations
    weightHist = cnn.weights;
    
    % train the nn
    disp(['Training Epoch: ' num2str((i-1) * cnn.params.epochs + 1)])
    [cnn.weights, trainerr] = cnntrain(cnn.layers, cnn.weights, cnn.params, single(X), single(y), cnn.funtype);  
    %disp([num2str(mean(trainerr(:, 1))*100) '% train error']);
    %err = testCNNWrap(cnn,X,y);
    disp(['Training Error: ' num2str(trainerr)]);
    iterCompleted = i;
    
	% compare the ammount learned this iteration
    weightTrainProgress = mean(abs(cnn.weights - weightHist));
    disp(['Change Per Weight: ' num2str(weightTrainProgress)]);
    
    % if it didn't learn much, stop training and give up
    if weightTrainProgress < classifierParams.weightTrainStopCriteria
        % stop training and exit the loop
        disp('The weight train progress was too small. Now stopping.');
        disp(weightTrainProgress);
        break;
    end
    
    % shrink stepsize parameters
    cnn.params.alpha = cnn.params.alpha * 0.98;
    cnn.params.beta = cnn.params.beta * 0.98;
end;

trainerr = trainerr(1);

disp('Done Training!');


end

