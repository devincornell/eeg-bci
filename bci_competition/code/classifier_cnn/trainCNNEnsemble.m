function [bestCNN] = trainCNNEnsemble(X,y,N,generations)
%TRAINCNNENSEMBLE Summary of this function goes here
%   Detailed explanation goes here

% N should be divisible by 3
assert(N/3 == floor(N/3));
setDiv = floor(N/3);

% set some parameters
epocNumLim = 50;
weightTrainLim = 1e-9;

% get constant variables
inputMapSize = size(X(:,:,1));
outputs = size(y,2);

% create random ensemble and increment seed counter
k = 1;
cnnEnsemble = newCNNEnsemble(inputMapSize,outputs,k:k+N-1);
disp('Creating new NN Ensemble with the following seed parameters:');
disp(k:k+N-1);
k = N;

for i = 1:generations
    disp(['Starting to run generation ' num2str(i)]);
    
    % reset results variable
    costResults = zeros(N,1);
    
    % train all ensembles to stop criteria, in paralell
    batchJobs = cell(N,1);
    for j = 1:N
        disp(['Starting process to train nn ' num2str(j)]);
        batchJobs{j} = batch(@trainCNNWrap,3,{cnnEnsemble{j},X,y,epocNumLim,weightTrainLim});
    end
    for j = 1:N
        % when the job is finished, store the results
        wait(batchJobs{j});
        outputData = fetchOutputs(batchJobs{j});
        cnnEnsemble{j} = outputData{1};
        costResults(j) = outputData{2};
        disp(['Subject ' num2str(j) ' has finished training.']);
        disp([num2str(outputData{3}) ' iterations were completed']);
    end
    
    % sort networks in order of success criteria
    [~,resultOrder] = sort(costResults);
    cnnEnsemble = cnnEnsemble(resultOrder);
    disp('These are the costs of each network:');
    disp(costResults');
    disp('The trained networks have the following result order: ');
    disp(resultOrder');
    
    
    % save top 1/3, run again (but switch order)
    % (don't need to do anything)
    
    % mutate top 1/3 and replace middle 2/3 with mutations
    for j = 1*setDiv+1:2*setDiv
        j0 = mod(j,setDiv);
        if j0 == 0
            j0 = 3;
        end
        j1 = mod(j+1,setDiv);
        if j1 == 0
            j1 = 3;
        end
        cnnEnsemble{j} = mutateCNN(cnnEnsemble{j0},cnnEnsemble{j1},5);
        disp(['Mutating subjects ' num2str(j0) ' and ' num2str(j1) ' to create subject ' num2str(j)]);
    end
    
    % replace bottom 1/3 with random seed networks
    disp('Generating new networks using the following seed values:');
    disp(k+1:k+setDiv);
    cnnEnsemble(2*setDiv+1:3*setDiv) = newCNNEnsemble(inputMapSize,outputs,k+1:k+setDiv);
    k = k + setDiv;
   
    
    % perform a test to see how well the best neural network can classify
    % the data
    
    err = testCNNWrap(cnnEnsemble{1},X,y);
    disp('Now testing best neural net on the data.');
    disp(err);
end

% the best cnn is the one that has been sorted to the top
bestCNN = cnnEnsemble{1};

end

