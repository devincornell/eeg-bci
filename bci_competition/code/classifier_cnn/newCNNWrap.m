function [cnn] = newCNNWrap(inputMapSize,outputs,seed)
%NEWCNNWRAP Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
    seed = 0;
end


cnn.funtype = 'gpu';
%cnn.funtype = 'cpu';
%cnn.funtype = 'matlab';


cnn.params.epochs = 1;
cnn.params.alpha = 0.1;
% this is the parameter for invariant backpropagation
% keep it 0 for standard backpropagation
cnn.params.beta = 0; 
cnn.params.momentum = 0.9;
cnn.params.lossfun = 'logreg';
cnn.params.shuffle = 1;
cnn.params.seed = seed;
cnn.params.shuffle = 1;
cnn.params.verbose = 0;
cnn.params.batchsize = 500;
cnn.dropout = 0;


% this one is sketchy
cnn.layers = {
  struct('type', 'i', 'mapsize', inputMapSize, 'outputmaps', 1)...%, 'outputmaps', nExamples)
  % remove the following layer in the Matlab version - it is not implemented there
  %struct('type', 'j', 'mapsize', [28 28], 'shift', [1 1], ...
  %       'scale', [1.40 1.40], 'angle', 0.10, 'defval', 0)  
  struct('type', 'c', 'filtersize', [3 3], 'outputmaps', 32)
  %struct('type', 'c', 'filtersize', [3 3], 'outputmaps', 16)
  %struct('type', 'c', 'filtersize', [10 2], 'outputmaps', 12, 'initstd', 20)
  %struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  %struct('type', 'c', 'filtersize', [5 5], 'outputmaps', 64, 'padding', [2 2])
  %struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  %struct('type', 'f', 'length', 256, 'dropout', cnn.dropout)
  struct('type', 'f', 'length', 256, 'dropout', cnn.dropout)
  struct('type', 'f', 'length', outputs, 'function', 'soft')
};

% this one for sure works
cnn.layers = {
  struct('type', 'i', 'mapsize', inputMapSize, 'outputmaps', 1)
  % remove the following layer in the Matlab version - it is not implemented there
  %struct('type', 'j', 'mapsize', [28 28], 'shift', [1 1], ...
  %       'scale', [1.40 1.40], 'angle', 0.10, 'defval', 0)  
  struct('type', 'c', 'filtersize', [50 2], 'outputmaps', 16, 'padding', [25 1])
  %struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  struct('type', 'c', 'filtersize', [3 3], 'outputmaps', 16, 'padding', [2 2])
  %struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  struct('type', 'f', 'length', 16, 'dropout', 0)
  struct('type', 'f', 'length', outputs, 'function', 'soft')
};


% kernel is too big
cnn.layers = {
  struct('type', 'i', 'mapsize', inputMapSize, 'outputmaps', 1)
  % remove the following layer in the Matlab version - it is not implemented there
  %struct('type', 'j', 'mapsize', [28 28], 'shift', [1 1], ...
  %       'scale', [1.40 1.40], 'angle', 0.10, 'defval', 0)
  struct('type', 'c', 'filtersize', [3 3], 'outputmaps', 16, 'padding', [2 2])
  %struct('type', 'c', 'filtersize', [2 2], 'outputmaps', 16)
  struct('type', 'c', 'filtersize', [2 2], 'outputmaps', 32)
  %struct('type', 's', 'scale', [2 2], 'function', 'max', 'stride', [2 2])
  %struct('type', 'c', 'filtersize', [3 3], 'outputmaps', 16, 'padding', [2 2])
  %struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  struct('type', 'f', 'length', 256, 'dropout', 0)
  struct('type', 'f', 'length', outputs, 'function', 'soft')
};


% store the weights in the cnn object
cnn.weights = single(genweights(cnn.layers, cnn.params, cnn.funtype));



end

