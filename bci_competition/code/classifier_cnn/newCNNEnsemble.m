function [cnnEnsemble] = newCNNEnsemble(inputMapSize,outputs,seeds)
%NEWCNNENSEMBLE Summary of this function goes here
%   Detailed explanation goes here

cnnEnsemble = cell(length(seeds),1);
for i = 1:length(seeds)
    cnnEnsemble{i} = newCNNWrap(inputMapSize,outputs,seeds(i));
end


end

