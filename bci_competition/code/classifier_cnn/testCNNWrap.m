function [successPercentage] = testCNNWrap(cnn,X,y)
%TESTCNNWRAPPER Summary of this function goes here
%   Detailed explanation goes here

[err, bad, pred] = cnntest(cnn.layers,cnn.weights,cnn.params,single(X),single(y),cnn.funtype);  
%disp([num2str(err*100) '% error']);  
%errors(i) = err;

successPercentage = 100*(1-err);

end

