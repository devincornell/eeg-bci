function [B] = eogAnalysis(info, signal, eog_channel_num, showplot)
%EOGANALYSIS Summary of this function goes here
%   Detailed explanation goes here

if nargin < 4
    showplot = 0;
end


% extract indexes from eyes closed, eyes open, and eye movement trials
preInd = getPretrialIndexes(info);

chan = size(signal,2);
eegChan = 1:chan-eog_channel_num;
eogChan = chan-eog_channel_num+1:chan;


% get transformation coefficients from EOG and ICA analysis
if numel(preInd) > 1
    [B] = preproc_coeff(signal(preInd(1,1):preInd(end,2),:),eegChan,eogChan);
else
    B = ones(chan,chan-eog_channel_num);
end

% compare before/after eog analysis
if showplot ~= 0
    figure(1);
    subplot(2,1,1);
    plot(series.s(preInd(1,1):preInd(3,2),:));
    title('Before Transformation');
    subplot(2,1,2);
    plot(series.s(preInd(1,1):preInd(3,2),:)*B);
    title('After Transformation');
end

end

