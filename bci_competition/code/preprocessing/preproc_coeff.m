function [ B ] = preproc_coeff( s, EEGcols, EOGcols )
%PRESESSION_ANALYSIS Summary of this function goes here
%   Detailed explanation goes here

%% take care of NaN values
% replace NaN's
s(isnan(s)) = 0;


%% remove effects of EOG
% The correction of a single record is obtained like this:      
%    [R,S2] = regress_eog(S1, EL, OL)
%    [R,S2] = regress_eog(filename, EL, OL)
%    [R,S2] = regress_eog(filename)
%     OL = IDENTIFY_EOG_CHANNELS(filename)
%     EL are all remaining channels

%   S1   recorded data
%   EL   list of eeg channels: those channels will be corrected   
%   OL   eog/ecg channels. 
%        if OL is a vector, it represents the list of noise channels 
%        if OL is a matrix, OL derives the noise channels through rereferencing. 
%           This is useful if the EOG is recorded monopolar, but the bipolar EOG 
%           should be used for for artefact reduction (because the global EEG should remain), 
%           One can define OL = sparse([23,24,25,26],[1,1,2,2],[1,-1,1,-1]) 
%        resulting in two noise channels defined as bipolar channels #23-#24 and #25-#26
%   R.r1, R.r0    rereferencing matrix for correcting artifacts with and without offset correction
%   R.b0    coefficients of EOG influencing EEG channels
%   S2   corrected EEG-signal      

[R,s] = regress_eog(s, EEGcols, EOGcols);


%% independant component analysis to separate components
% [A, W] = FASTICA (mixedsig); gives only the estimated mixing matrix
% A and the separating matrix W.
%[~, W] = fastica(s(:,EEGcols)', 'verbose', 'off');


%% set coefficients
%Aeog = R.r0;
%Aica = [W'; zeros(length(EOGcols),length(EEGcols))];

%B = Aeog*Aica;
B = R.r0(:,EEGcols);



end

