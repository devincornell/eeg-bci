function [X,y] = generateCSPFeatures(info, signal, classlabels, B, W, bands, window, overlap)
%TRIALANALYSIS Summary of this function goes here
%   Detailed explanation goes here

% set default window and overlap sizes
if nargin < 3
    B = ones(25,22);
end
if nargin < 4
    window = 250;
end
if nargin < 5
    overlap = 3;
end

% get signal indices for each trial
[ind,labels] = getSegmentTrialIndexes(info, classlabels, window, overlap);

X = zeros(size(ind,1), length(calculateCSPFeatures(signal(ind(1,1):ind(1,2),:)*B,W,bands)));
for i = 1:size(ind,1);
        % add to labeled observations
        X(i,:) = calculateCSPFeatures(signal(ind(i,1):ind(i,2),:)*B,W,bands)';
end

y = labels;



end

