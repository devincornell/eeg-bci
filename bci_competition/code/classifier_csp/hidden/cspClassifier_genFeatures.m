function [features] = cspClassifier_genFeatures(X,cspMatrix,bands)
%CSPCLASSIFIER_GENFEATURES Summary of this function goes here
%   Detailed explanation goes here

% adjust this for preallocation of input
feat_size = 27;

% first dimension is time, second dimension is channels, third dimension is
% frequency, and fourth dimension is training examples
% separate training examle X into frequency bands using filters
S = filterBands(X,bands);

% generate features to use as inputs to the mlp
features = zeros(size(X,3),feat_size);

% iterate over every training example
for i = 1:size(S,3)
    feature_example = zeros(0);
    
    % gen features from each frequency band
    for j = 1:size(S,4)
        sbar = cspMatrix(:,:,j)'*S(:,:,i,j)'*S(:,:,i,j)*cspMatrix(:,:,j);

        f = log( diag(sbar) / trace(sbar) );
        feature_example = [feature_example; f(:)];
    end
    features(i,:) = feature_example';
end

end

