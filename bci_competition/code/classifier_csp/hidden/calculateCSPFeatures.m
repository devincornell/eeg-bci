function [x] = calculateCSPFeatures(s,W,bands)
%FEATURESCSP Summary of this function goes here
%   Detailed explanation goes here

% split s into desired frequency bands and create the filters
S = filterBands(s,bands);

x = [];

for i = 1:size(S,3)
    sbar = W(:,:,i)'*S(:,:,i)'*S(:,:,i)*W(:,:,i);
    
    features = log( diag(sbar) / trace(sbar) );
    x = [x; features(:)];
end

end

