function [ S ] = getClassSignals(info, signal, B)
%GETCLASSSIGNALS Summary of this function goes here
%   Detailed explanation goes here

% get signal indices for each trial
[ind,labels] = getTrialIndexes(info);

% convert label to class number
y = labels-768;

class = unique(y);
S = cell(length(class),1);

% go through each of the possible classes
for i = 1:length(class);
    
    % calculate the total number of samples belonging to the class
    classPoints = sum(ind(y==class(i),2)-ind(y==class(i),1));
    
    % init matrix to store signals from a given class
    S{i} = zeros(classPoints,3);

    % go through each index that belongs to the given class
    cI = find(y==class(i));
    k = 1;
    for j = 1:length(cI)
        trialPoints = ind(cI(j),2)-ind(cI(j),1);
        S{i}(k:k+trialPoints,:) = signal(ind(cI(j),1):ind(cI(j),2),:)*B;
        k = k + trialPoints+1;
    end
end


end

