function [x] = featureSpectralCSP(s,W,bands)
%FEATURESCSP Summary of this function goes here
%   Detailed explanation goes here

% split s into desired frequency bands and create the filters
S = filterBands(s,bands);

x = zeros(3,1,size(S,3));

for i = 1:size(S,3)
    sbar = W(:,:,i)'*S(:,:,i)'*S(:,:,i)*W(:,:,i);
    
    features = log( diag(sbar) / trace(sbar) );
    x(:,1,i) = features(:);
end

end
