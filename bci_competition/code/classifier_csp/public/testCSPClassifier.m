function [percent,classPercents] = testCSPClassifier(cspc,X,y)
%EVALUATECSPCLASSIFIER Summary of this function goes here
%   Detailed explanation goes here

y = convertClassLabel(y);

features = cspClassifier_genFeatures(X,cspc.cspMatrix,cspc.bands);

[percent,classPercents] = accuracyTestMLP(cspc.mlp,features,y);

end

