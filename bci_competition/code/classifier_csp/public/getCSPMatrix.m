function [V] = getCSPMatrix(X,y,bands)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

class_num = length(unique(y));
class_enum = unique(y);

% combine the signals based on the label
Xbar = cell(class_num,1);
for i = 1:class_num
    % determine which samples are in the current class
    inClass = y == class_enum(i);
    
    % extract the signals, I know this is crazy
    dimOrder = [1 flip(2:size(X,2))];
    Xbar{i,1} = reshape(permute(X(:,:,inClass),dimOrder),[],size(X,2));
end

% allocate space for band-separated spatial transform matricies
V = zeros(size(X,2),size(X,2),length(bands)-1);

% create a csp matrix for each band
for i = 1:length(bands)-1
    % filter the data
    Fc = [bands(i) bands(i+1)];
    
    S1 = cov(bwfilt(Xbar{1},Fc));
    S2 = cov(bwfilt(Xbar{2},Fc));
    
    % apply CSP to signal
    V(:,:,i) = cspl2(S1,S2);
end

