function [cspc] = trainCSPClassifier(classifierArgs, X, y)
%TRAINCSPCLASSIFIER X is <n by p by examples> matrix that contains
%p-channel signal snippets times the number of examples

% record number of classes
num_classes = size(y,2);

% get the form of y which is just the class number it belongs to y
y = convertClassLabel(y);

% generate CSP matrix
cspMatrix = getCSPMatrix(X,y,classifierArgs.bands);

% generate features for csp
features = cspClassifier_genFeatures(X,cspMatrix,classifierArgs.bands);

% create a new csp classifier
cspc = newCSPClassifier([size(features,2) classifierArgs.hiddenLayerSize num_classes],cspMatrix,classifierArgs.bands);

% features are
% feed processed features into neural network
cspc.mlp = learnMLP(cspc.mlp,features,y,classifierArgs.trainIterations);

end

