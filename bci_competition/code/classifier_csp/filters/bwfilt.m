function [ xbar ] = bwfilt(x,fn)
%BWFILT Summary of this function goes here
%   Detailed explanation goes here

[b,a] = butter(2,fn*2,'bandpass');
xbar = filtfilt(b,a,x);

