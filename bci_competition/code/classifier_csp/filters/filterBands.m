function [S] = filterBands(X,bands)
%FILTERBANDS Summary of this function goes here
%   Detailed explanation goes here

% preallocate S
S = zeros(size(X,1),size(X,2),size(X,3),length(bands)-1);

% band-pass each band
for i = 1:length(bands)-1
    [b,a] = butter(2,[bands(i) bands(i+1)]*2,'bandpass');
    S(:,:,:,i) = filtfilt(b,a,X);
end

end

