function [y] = morletWaveletTransform(x,f,n)
%WAVEFUNCTION Summary of this function goes here
%   Detailed explanation goes here

s = n/(2*pi*f);

t = (1:size(x,1))-size(x,1)/2;

h = exp(2*pi*1j*f*t).*exp(-t.^2/(2*s^2));

y = zeros(size(x));

for i = 1:size(x,2)
    y(:,i) = conv(x(:,i),h,'same');
end

end

