function [feat] = calculateWaveletCovarianceFeatures(x)
%CALCULATEWAVELETCOVARIANCE Summary of this function goes here
%   Detailed explanation goes here

% choose the bands which are relevant
bands = (2:2:40)/250;

% create a layer for y for every frequency band
%y = zeros(size(x,1),size(x,2),length(f));


feat = zeros(0);

% convolve the signal with each of the wavelets
for i = 1:length(bands)
    y = morletWaveletTransform(x,bands(i),6);
    
    c = (y'*y)/(size(y,1)-1);
    c = [real(c(:)); imag(c(:))];
    feat = [feat; c(:)];
end



end

