function [features] = waveletClassifier_genFeatures(wc,X)
%WAVELETCLASSIFIER_GENFEATURES Summary of this function goes here
%   Detailed explanation goes here

features = zeros(size(X,3),18);

for i = 1:size(X,3)
    xbar = X(:,:,i);
    
    y = morletWaveletTransform(xbar,13/250,6);
    
    f1 = cov(abs(y));
    f2 = cov(angle(y));
    
    features(i,:) = [f1(:); f2(:)]';
end



end

