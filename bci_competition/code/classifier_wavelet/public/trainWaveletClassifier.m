function [wc] = trainWaveletClassifier(wc,X,y,n)
%TRAINWAVELETCLASSIFIER Summary of this function goes here
%   Detailed explanation goes here

features = waveletClassifier_genFeatures(wc,X);


wc.mlp = learnNN(wc.mlp,features,y,n);

end

