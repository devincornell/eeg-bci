function [percent,classPercents] = evaluateWaveletClassifier(wc,X,y)
%EVALUATEWAVELETCLASSIFIER Summary of this function goes here
%   Detailed explanation goes here

features = waveletClassifier_genFeatures(wc,X);

[percent,classPercents] = accuracyTestNN(wc.mlp,features,y);


end

