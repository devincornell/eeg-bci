
%% score classifications for individuals
resultFileNum = 27;
results = zeros(27,2);
for i = 12:12
    load(['results/train_progress_' num2str(i) '.mat'],'X','y')

    nn = newNN([size(X,2) 100 10 length(unique(y))]);
    nn = learnNN(nn,X,y,250);

    [~,p] = accuracyTestNN(nn,X,y);

    results(i,:) = p(:,2)';
end


%% score classifications for all users

nna = newNN([size(X,2) 10 length(unique(y))]);

resultFileNum = 27;
results = zeros(resultFileNum,2);
bestFeatures = zeros(resultFileNum,5);
Xb = [];
yb = [];
for i = 1:resultFileNum
    load(['results/train_progress_' num2str(i) '.mat'],'X','y')
    Xb = [Xb; X];
    yb = [yb; y];
    
    nna = learnNN(nn,X,y,100);

    [~,p] = accuracyTestNN(nna,X,y);
    results(i,:) = p(:,2)';
    
    [~,ind] = featEvalNN(nna);
    bestFeatures(i,:) = ind(1:5);
end

nnb = newNN([size(Xb,2) 10 length(unique(yb))]);

nnb = learnNN(nnb,Xb,yb,250);

[~,p] = accuracyTestNN(nnb,Xb,yb);


%% evaluate the state of a given neural network

[feats(:,1),feats(:,2)] = featEvalNN(nn);