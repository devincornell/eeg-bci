
folder = '\results\';
files = dir([pwd folder])';

ranks = zeros(27,27);

sumHeader = {'Trial', 'Accuracy','Class Accuracy', 'Best Features'};
summary = {};

i = 1;
for file = files
    if file.isdir == 0
        load(file.name);
        
        ind1 = (y==1);
        ind2 = (y==2);

        mu1 = mean(X(ind1,:),1);
        mu2 = mean(X(ind2,:),1);

        % find standard deviations per class normalized by total std
        std1 = std(X(ind1,:),0,1)./std(X,0,1);
        std2 = std(X(ind2,:),0,1)./std(X,0,1);

        scores = abs(mu1-mu2)./(std1.*std2);

        [~,ranks(i,:)] = sort(scores);
    
        % put information into summary cell array
        summary{i,1} = fname;
        summary{i,2} = p;
        summary{i,3} = mat2str(classPercent);
        summary{i,4} = mat2str(ranks(i,end-1:end));
        
        i = i + 1;
    end
end

summary = [sumHeader; summary];

