
resultsFile = 'results/train_progress_24.mat';

load(resultsFile);

ind1 = (y==1);
ind2 = (y==2);

mu1 = mean(X(ind1,:),1);
mu2 = mean(X(ind2,:),1);

% find standard deviations per class normalized by total std
std1 = std(X(ind1,:),0,1)./std(X,0,1);
std2 = std(X(ind2,:),0,1)./std(X,0,1);

scores = abs(mu1-mu2)./(std1.*std2);

[~,ranks] = sort(scores);

xfeat = 3;
yfeat = 18;

scatter(X(ind1,xfeat),X(ind1,yfeat));
hold on;
scatter(X(ind2,xfeat),X(ind2,yfeat));
hold off;

