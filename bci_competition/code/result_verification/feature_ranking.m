
resultsFile = 'results/train_progress_1.mat';


folder = '\results\';
files = dir([pwd folder])';

ranks = zeros(27,27);

i = 1;
for file = files
    if file.isdir == 0
        load(file.name);

        ind1 = (y==1);
        ind2 = (y==2);

        mu1 = mean(X(ind1,:),1);
        mu2 = mean(X(ind2,:),1);

        % find standard deviations per class normalized by total std
        std1 = std(X(ind1,:),0,1)./std(X,0,1);
        std2 = std(X(ind2,:),0,1)./std(X,0,1);

        scores = abs(mu1-mu2)./(std1.*std2);

        [~,ranks(i,:)] = sort(scores);
    
        i = i + 1;
    end
end
