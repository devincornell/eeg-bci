function [testInfo] = testPatientClassifier2b(testParams, classifierParams, trainClassifierFunc, testClassifierFunc)
%TESTCLASSIFIER2B Summary of this function goes here
%   Detailed explanation goes here

% get names of files and correct folders for data
gdf_files = get2bFiles();
data_folder = '\data\BCICIV_2b_gdf\';

% generate results cell
testInfo = struct();
testInfo.results = zeros();
testInfo.testParams = testParams;
testInfo.classifierParams = classifierParams;

% go through each test subject
for i = 1:size(gdf_files,1)
    
    disp('==============================================');
    disp(['Now performing analysis for ' gdf_files{i,1}]);
    
    % for each of three training sessions of a given patient
    X = zeros(0,0,0);
    y = zeros(0);
    m = 1;
    for j = 1:3
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,j} '.gdf']);
        load([gdf_files{i,j} '.mat'], 'classlabel');

        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);
        
        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, testParams.window, testParams.overlap);

        % extract labeled signal windows
        feat = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations (add in 3rd dimension)
            X(:,:,m) = signal(ind(k,1):ind(k,2),:)*B;
            m = m + 1;
        end
        
        y = [y; labels];
        
    end
    
    % create ybar to act as labels for the data
    num_classes = max(unique(y));
    ybar = repmat(1:num_classes,size(y,1),1) == repmat(y,1,num_classes);
    X(isnan(X)) = 0;
    
    
    % ================ Classifier Portion ==================%
    % construct a trained classifier
    classifierObj = trainClassifierFunc(classifierParams,X,ybar);
    
    % store training percentage and classifier object
    testInfo.results(i,1) = testClassifierFunc(classifierObj,X,ybar);
    testInfo.classifier{i,1} = classifierObj;
    % ================ Classifier Portion End ==============%
    
    % save in case of error
    save(['results/intermediate_results.mat'], 'testInfo');
    
    % for each of 2 evaluation sessions of a given patient
    for j = 1:2
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,3+j} '.gdf']);
        load([gdf_files{i,3+j} '.mat'], 'classlabel');
        
        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);

        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, testParams.window, testParams.overlap);

        % get features for each of the trials
        X = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations
            X(:,:,k) = signal(ind(k,1):ind(k,2),:)*B;
        end
        
        % create ybar to act as labels for the data
        ybar = repmat(1:num_classes,size(labels,1),1) == repmat(labels,1,num_classes);
        X(isnan(X)) = 0;
        
        % ================== Classifier Test ====================== %
        % test out the trained nn on the evaluation data
        testInfo.results(i,1+j) = testClassifierFunc(classifierObj, X, ybar);
        % ================== Classifier Test End ================== %
    end
end





end

