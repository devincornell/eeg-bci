
% get names of files and correct folders for data
gdf_files = get2bFiles();
data_folder = '\data\BCICIV_2b_gdf\';
label_folder = 'data\true_labels_b\';

% specify other parameters
window = 250;
overlap = 3;
bands = (4:4:40)/250;
trainIterations = 200;

% generate results cell
testInfo = struct();
testInfo.window = window;
testInfo.overlap = overlap;
testInfo.bands = bands;
testInfo.trainIterations = trainIterations;
testInfo.results = zeros();

% go through each test subject
for i = 1:size(gdf_files,1)
    
    disp('==============================================');
    disp(['Now performing analysis for ' gdf_files{i,1}]);
    
    % for each of three training sessions of a given patient
    X = zeros(0,0,0);
    y = zeros(0);
    m = 1;
    for j = 1:3
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,j} '.gdf']);
        load([gdf_files{i,j} '.mat'], 'classlabel');

        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);
        
        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, window, overlap);

        % extract labeled signal windows
        feat = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations (add in 3rd dimension)
            X(:,:,m) = signal(ind(k,1):ind(k,2),:)*B;
            m = m + 1;
        end
        
        y = [y; labels];
        
    end
    
    % whiten each signal x by subtracting the mean and multiplying by the
    % variance
    for j = 1:size(X,3)
        % subtract the mean
        X(:,:,j) = X(:,:,j) - repmat(mean(X(:,:,j),1),size(X,1),1);
        
        % give an average standard deviation of one
        %X(:,:,j) = X(:,:,j)./repmat(var(X(:,:,j),1),size(X,1),1);
    end
    
    % create ybar to act as labels for the data
    num_classes = max(unique(y));
    ybar = repmat(1:num_classes,size(y,1),1) == repmat(y,1,num_classes);
    X(isnan(X)) = 0;
    
    % construct a cnn classifier
    %cnn = trainCNNEnsemble(X(:,:,1:50),ybar(1:50,:),9,10);
    cnn = newCNNWrap(size(X(:,:,1)),size(ybar,2),0);
    cnn = trainCNNWrap(cnn,X(:,:,:),ybar(:,:),150,1e-11);
    testInfo.results(i,1) = 100-testCNNWrap(cnn, X, ybar)*100
    
    % train the neural net on the training data
    %trainPercent = 100*(1-er)
    %testInfo.trainIterations = trainIterations;
    %testInfo.results(i,1) = trainPercent;
    testInfo.classifier{i,1} = cnn;
    
    save(['results/ ' gdf_files{i,1} '.mat'], 'cnn');
    
    % for each of 2 evaluation sessions of a given patient
    for j = 1:2
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,3+j} '.gdf']);
        load([gdf_files{i,3+j} '.mat'], 'classlabel');
        
        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);

        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, window, overlap);

        % get features for each of the trials
        X = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations
            X(:,:,k) = signal(ind(k,1):ind(k,2),:)*B;
        end
        
        % create ybar to act as labels for the data
        ybar = repmat(1:num_classes,size(labels,1),1) == repmat(labels,1,num_classes);
        X(isnan(X)) = 0;
        
        % test out the trained nn on the evaluation data
        [er, bad] = testCNNWrap(cnn, X, ybar);
        testPercent = 100*(1-er)
        testInfo.results(i,1+j) = testPercent;
    end
end

save('results/results.mat', 'testInfo');
