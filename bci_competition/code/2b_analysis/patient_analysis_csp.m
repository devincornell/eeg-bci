
% get names of files and correct folders for data
gdf_files = get2bFiles();
data_folder = '\data\BCICIV_2b_gdf\';
label_folder = 'data\true_labels_b\';

% specify other parameters
window = 999;
overlap = 3;
bands = (4:4:40)/250;
trainIterations = 200;

% generate results cell
testInfo = struct();
testInfo.window = window;
testInfo.overlap = overlap;
testInfo.bands = bands;
testInfo.trainIterations = trainIterations;
testInfo.results = zeros();

% go through each test subject
for i = 1:size(gdf_files,1)
    
    % for each of three training sessions of a given patient
    X = zeros(0,0,0);
    y = zeros(0);
    m = 1;
    for j = 1:3
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,j} '.gdf']);
        load([gdf_files{i,j} '.mat'], 'classlabel');

        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);
        
        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, window, overlap);

        % extract labeled signal windows
        feat = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations (add in 3rd dimension)
            X(:,:,m) = signal(ind(k,1):ind(k,2),:)*B;
            m = m + 1;
        end
        
        y = [y; labels];
        
    end
    
    % generate csp matrix that classifier should use
    cspMatrix = getCSPMatrix(X,y,bands);
    
    % train the neural net on the training data
    cspc = newCSPClassifier([27 50 2],cspMatrix,bands);
    cspc = trainCSPClassifier(cspc,X,y,trainIterations);
    trainPercent = evaluateCSPClassifier(cspc,X,y);
    trainPercent
    testInfo.trainIterations = trainIterations;
    testInfo.results(i,1) = trainPercent;
    testInfo.classifier = cspc;
    
    % for each of 2 evaluation sessions of a given patient
    for j = 1:2
        % extract data from files
        [info,signal] = gdfFileRead([pwd data_folder gdf_files{i,3+j} '.gdf']);
        load([gdf_files{i,3+j} '.mat'], 'classlabel');
        
        % perform EOG preprocessing, get the transform matrix B
        B = eogAnalysis(info,signal,3);

        % get signal indices for each trial
        [ind,labels] = getSegmentTrialIndexes(info, classlabel, window, overlap);

        % get features for each of the trials
        X = zeros(0);
        for k = 1:size(ind,1);
            % add to labeled observations
            X(:,:,k) = signal(ind(k,1):ind(k,2),:)*B;
        end
        
        % test out the trained nn on the evaluation data
        [percent, classPercents] = evaluateCSPClassifier(cspc,X,labels);
        percent
        testInfo.results(i,1+j) = percent;
    end
    
end

save('results/results.mat', 'testInfo');
