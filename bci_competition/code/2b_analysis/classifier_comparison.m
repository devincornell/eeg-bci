
%% Classifier Test
%classifierObj = trainClassifierFunc(classifierParams,X,ybar);
%classificationPercentage = testClassifierFunc(classifierObj,X,ybar);

%% set test parameters
testParams = struct();
testParams.window = 250;
testParams.overlap = 3;

% set arguments for classifier construction
classifierArgs = struct();
classifierArgs.trainIterations = 50;


%% test the csp classifier

classifierArgs.bands = (4:4:40)/250;
classifierArgs.hiddenLayerSize = 20;
testResult = testPatientClassifier2b(testParams,classifierArgs,@trainCSPClassifier,@testCSPClassifier);

save('results/csp_result.mat','testResult');



%% test the cnn classifier

classifierArgs.weightTrainStopCriteria = 1e-11;
testResult = testPatientClassifier2b(testParams,classifierArgs,@trainCNNWrap,@testCNNWrap);

save('results/cnn_result.mat','testResult');



