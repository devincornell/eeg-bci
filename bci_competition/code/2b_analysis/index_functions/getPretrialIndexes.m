function [indexes] = getPretrialIndexes(info)
%GETPRETRIALINDEXES Summary of this function goes here
%   Detailed explanation goes here

indexes = [];

evs = [info.EVENT.TYP info.EVENT.POS info.EVENT.DUR];

% eyes open trial
ev = evs(evs(:,1)==276,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end

% eyes closed trial
ev = evs(evs(:,1)==277,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end


% eye movement trial
ev = evs(evs(:,1)==1072,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end

% eye horizontal movement trial
ev = evs(evs(:,1)==1077,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end

% eye vertical movement trial
ev = evs(evs(:,1)==1078,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end

% eye rotation movement trial
ev = evs(evs(:,1)==1079,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end

% eye blink movement trial
ev = evs(evs(:,1)==1081,:);
if numel(ev) > 0
    indexes = [indexes; [ev(2), ev(2)+ev(3)-1]];
end



end

