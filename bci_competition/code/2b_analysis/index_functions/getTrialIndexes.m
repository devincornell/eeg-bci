function [indexes, labels] = getTrialIndexes(info)
%GETLABELEDINDEXES Summary of this function goes here
%   Detailed explanation goes here

% get information about each event
evs = [info.EVENT.TYP info.EVENT.POS info.EVENT.DUR];

% get trial indexes
trials = find(evs(:,1)==768)';

% get trial indexes and labels
indexes = [];
labels = [];
for i = trials;
    
    % get the event information and determine the type of cue
    trialStart = evs(i,2)+1000;
    trialLength = 1000;%evs(i,3);
    trialEnd = trialStart+trialLength-1;
    
    indexes = [indexes; trialStart, trialEnd];
    labels = [labels; evs(i+1,1)];
    
end

end

