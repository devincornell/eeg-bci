function [indexes, labels] = getSegmentTrialIndexes(info,classlabels,window,overlap)
%GETLABELEDINDEXES Summary of this function goes here
%   Detailed explanation goes here

% set the window length and overlap
if nargin < 2
    window = 250;
end
if nargin < 3
    overlap = 3;
end


% get information about each event
evs = [info.EVENT.TYP info.EVENT.POS info.EVENT.DUR];

% get trial indexes
trials = find(evs(:,1)==768)';

% get trial indexes and labels
indexes = [];
labels = [];
k = 1;
for i = trials;
    
    % get the event information and determine the type of cue
    trialStart = evs(i,2)+1000;
    trialLength = 1000;%evs(i,3);
    
    % loop through each window length of samples in the trial
    for j = 1:floor(window/overlap):(trialLength-window)
        indexes = [indexes; [trialStart-1+j, trialStart-1+j-1+window]];
        labels = [labels; classlabels(k)];%[labels; evs(i+1,1)];
    end
    
    k = k + 1;
end

end

