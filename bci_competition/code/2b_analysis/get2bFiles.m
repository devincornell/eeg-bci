function [gdf_files] = get2bFiles()
%GET2BFILES Summary of this function goes here
%   Detailed explanation goes here


gdf_files = {...
'B0101T', 'B0102T', 'B0103T', 'B0104E', 'B0105E';...
'B0201T', 'B0202T', 'B0203T', 'B0204E', 'B0205E';...
'B0301T', 'B0302T', 'B0303T', 'B0304E', 'B0305E';...
'B0401T', 'B0402T', 'B0403T', 'B0404E', 'B0405E';...
'B0501T', 'B0502T', 'B0503T', 'B0504E', 'B0505E';...
'B0601T', 'B0602T', 'B0603T', 'B0604E', 'B0605E';...
'B0701T', 'B0702T', 'B0703T', 'B0704E', 'B0705E';...
'B0801T', 'B0802T', 'B0803T', 'B0804E', 'B0805E';...
'B0901T', 'B0902T', 'B0903T', 'B0904E', 'B0905E';...
};

end

