

folder = '\data\BCICIV_2a_gdf\train\';
files = dir([pwd folder]);

k = 1;
for file = files'
    if file.isdir == 0
        
        disp(['Loading Data from ' file.name]);
        
        % load data
        [info,signal] = gdfFileRead([pwd folder file.name]);
        
        disp(['Performing EOG and ICA Preprocessing on ' file.name]);
        
        % perform EOG preprocessing, and get the transform matrix B
        B = eogAnalysis(info,signal,3);

        disp(['Extracting Features from ' file.name]);
        
        % build feature matrix
        window = 250;
        overlap = 2;
        [X,y] = trialAnalysis(info,signal,B,window,overlap);

        % remove unlabeled trials
        X = X(y~=255 & y~=15,:);
        y = y(y~=255 & y~=15);
        
        % learn from the dataset
        if ~isempty(y)
            
            % create new nn on first iteration
            if k == 1
                nn = newNN([size(X,2) 25 4]);
            end
                
            disp(['Training Neural Network on ' file.name]);

            [nn,cost] = learnNN(nn,X,y,100);

            disp(['Checking training progress of ' file.name]);

            % determine accuracy of datasetlol
            [p,classPercent] = accuracyTestNN(nn,X,y);
            disp(classPercent);

            disp(['Saving NN training after ' file.name]);

            fname = file.name;
            save(['results/train_progress_' num2str(k) '.mat'],'nn','X','y','B','fname','classPercent');
        end
        
        k = k + 1;
    end
end




