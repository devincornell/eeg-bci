
bands = (4:4:40)/250;
folder = '\data\BCICIV_2b_gdf\train\';
files = dir([pwd folder]);


k = 1;
for file = files'
    if file.isdir == 0
        
        disp(['Loading Data from ' file.name]);
        
        % load data and data labels
        [info,signal] = gdfFileRead([pwd folder file.name]);
        load([file.name(1:end-3) 'mat'], 'classlabel');
        
        disp(['Performing EOG and ICA Preprocessing on ' file.name]);
        
        % perform EOG preprocessing, and get the transform matrix B
        B = eogAnalysis(info,signal,3);

        % perform common spatial pattern analysis
        W = getCSPMatrix(info, signal, B, bands);
        
        disp(['Extracting Features from ' file.name]);
        
        % build feature matrix
        window = 249;
        overlap = 3;
        [X,y] = generateCSPFeatures(info,signal,classlabel,B,W,bands,window,overlap);

        % remove unlabeled trials
        %X = X(y~=255 & y~=15,:);
        %y = y(y~=255 & y~=15);
        
        % learn from the dataset
        if ~isempty(y)
            
            % on first iteration create a new neural network
            if k == 1
                nn = newNN([size(X,2) 30 2]);
            end
            
            disp(['Training Neural Network on ' file.name]);

            [nn,cost] = learnNN(nn,X,y,500);

            disp(['Checking training progress of ' file.name]);

            % determine accuracy of datasetlol
            [p,classPercent] = accuracyTestNN(nn,X,y);
            disp(p);
            disp(classPercent);

            disp(['Saving NN training after ' file.name]);

            fname = file.name;
            save(['results/train_progress_' num2str(k) '.mat'],'nn','X','y','B','p','fname','classPercent');
        end
        
        k = k + 1;
    end
end




